package net.hypercube.games.minimal.lights;

import org.joml.Vector3d;

public class Sun extends DirectionalLight {
	public Sun() {
		super(new Vector3d(-20, -10, -5));
	}
}
