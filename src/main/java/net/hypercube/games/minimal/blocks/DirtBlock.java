package net.hypercube.games.minimal.blocks;

import org.joml.Vector3i;

import net.hypercube.api.assets.material.PhongMaterial;
import net.hypercube.api.assets.texture.Texture2D;
import net.hypercube.api.blocks.CubeBlock;

public class DirtBlock extends CubeBlock {
	private static final Texture2D DIFFUSE_MAP = new Texture2D("assets/Textures/dirt.png");
	private static final PhongMaterial MATERIAL = PhongMaterial.builder().diffuseMap(DIFFUSE_MAP).build();

	public DirtBlock(Vector3i position) {
		super(MATERIAL, position);
	}
}
