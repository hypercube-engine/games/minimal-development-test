package net.hypercube.games.minimal;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.hypercube.api.blocks.IBlockManager;
import net.hypercube.api.gameobjects.entities.Player;
import net.hypercube.api.gameobjects.illumination.Illuminating;
import net.hypercube.api.games.IGame;
import net.hypercube.api.worldgenerators.IWorldGenerator;
import net.hypercube.games.minimal.blocks.DirtBlock;
import net.hypercube.games.minimal.blocks.WoodBlock;
import net.hypercube.worldgenerators.flatworldgenerator.FlatWorldGenerator;

/**
 *
 * @author Christian Danscheid
 */
public class MainGame implements IGame {
	final static Logger log = LoggerFactory.getLogger(MainGame.class);

	@Override
	public String getName() {
		return "Main Game";
	}

	@Override
	public void registerBlocks(IBlockManager blockManager) {
		log.trace("Registering blocks");

		blockManager.registerBlock(DirtBlock.class);
		blockManager.registerBlock(WoodBlock.class);

		blockManager.registerAlias(DirtBlock.class, FlatWorldGenerator.RequiredAlias.DIRT);
	}

	@Override
	public IWorldGenerator getWorldGenerator(IBlockManager blockManager, Integer seed) {
		log.trace("Creating world generator using seed {}", seed);

		// return new FlatWorldGenerator(blockManager);
		return null;
	}

	@Override
	public Set<Illuminating> getWorldLights() {
		var lights = new HashSet<Illuminating>();

//		lights.add(new Sun());
//		lights.add(new AmbientLight(.25f));

		return lights;
	}

	@Override
	public Player onPlayerJoining(String name) {
		log.trace("Creating Entity for player {}", name);
//		return new PlayerEntity(name, new Vector3d(0, -4.5, 0));
		return null;
	}

}
