module net.hypercube.games.minimal {
	requires org.slf4j;
	requires net.hypercube.api;

	requires net.hypercube.worldgenerators.flatworldgenerator;
}